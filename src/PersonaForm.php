<?php

namespace Drupal\persona;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base form for persona edit forms.
 */
class PersonaForm extends EntityForm {

  /**
   * The persona entity storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $storage;

  /**
   * Constructs a PersonaForm object.
   *
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity type manager.
   */
  public function __construct(EntityStorageInterface $storage) {
    $this->storage = $storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')->getStorage('persona')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    /** @var \Drupal\persona\Entity\Persona $persona */
    $persona = $this->entity;
    $personas = $this->storage->loadMultiple();

    $form['label'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Name'),
      '#required' => TRUE,
      '#size' => 40,
      '#maxlength' => 80,
      '#default_value' => $persona->label(),
      '#description' => $this->t('The human-readable name is shown in persona lists and forms.'),
    );
    $form['id'] = array(
      '#type' => 'machine_name',
      '#default_value' => $persona->id(),
      '#machine_name' => array(
        'source' => array('label'),
        'exists' => array($this->storage, 'load'),
      ),
    );
    $form['description'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Description'),
      '#required' => TRUE,
      '#size' => 40,
      '#maxlength' => 80,
      '#default_value' => $persona->getDescription(),
      '#description' => $this->t('A short description of the persona.'),
    );
    $next = count($personas) + 1;
    $form['weight'] = array(
      '#type' => 'weight',
      '#title' => $this->t('Weight'),
      '#required' => TRUE,
      '#delta' => $next,
      '#default_value' => $persona->getWeight() ?: $next,
      '#description' => $this->t('The sort order for this record. Lower values display first.'),
    );
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $persona = $this->entity;
    if ($persona->isNew()) {
      drupal_set_message($this->t('Persona record created.'));
    }
    else {
      drupal_set_message($this->t('Persona record updated.'));
    }
    $persona->save();
    $form_state->setRedirect('persona.collection');
  }

  /**
   * {@inheritdoc}
   */
  public function delete(array &$form, FormStateInterface $form_state) {
    $persona = $this->entity;
    $persona->delete();
    $form_state->setRedirect('persona.collection');
  }

}
