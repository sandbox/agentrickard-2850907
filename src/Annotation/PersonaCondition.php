<?php

/**
 * @file
 * Contains \Drupal\persona\Annotation\PersonaCondition.
 */

namespace Drupal\persona\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a persona condition annotation object.
 *
 * Plugin Namespace: Plugin\PersonaCondition
 *
 * For a working example, see
 * \Drupal\persona\Plugin\PersonaCondition\ABTest.
 *
 * @see plugin_api
 *
 * @Annotation
 */
class PersonaCondition extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The modules required by the plugin.
   *
   * @var array
   */
  public $modules;

  /**
   * The human-readable name of the plugin.
   *
   * @ingroup plugin_translatable
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public $label;

  /**
   * A brief description of the hierarchy source.
   *
   * This will be shown when adding or configuring conditions.
   *
   * @ingroup plugin_translatable
   *
   * @var \Drupal\Core\Annotation\Translation (optional)
   */
  public $description = '';

}
