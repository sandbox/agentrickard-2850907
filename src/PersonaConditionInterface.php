<?php

namespace Drupal\persona;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining a persona condition entity.
 */
interface PersonaConditionInterface extends ConfigEntityInterface {

  /**
   * Returns the persona id associated with this condition.
   *
   * @return string
   *   A persona id.
   */
  public function getPersona();

  /**
   * Sets the persona id to be stored with this condition.
   *
   * @param string $id
   *   The persona id the condition is assigned to.
   *
   * @return $this
   */
  public function setPersona($id);

  /**
   * Returns the plugin for this condition.
   *
   * @return string
   *   A condition plugin.
   */
  public function getPlugin();

  /**
   * Sets the plugin for this condition.
   *
   * @param string $plugin
   *   The plugin of this condition.
   *
   * @return $this
   */
  public function setPlugin($plugin);

  /**
   * Returns the group for this condition.
   *
   * @return string
   *   A condition group.
   */
  public function getGroup();

  /**
   * Sets the group for this condition.
   *
   * @param string $group
   *   The group of this condition.
   *
   * @return $this
   */
  public function setGroup($group);

  /**
   * Returns the logic for this condition.
   *
   * @return string
   *   A condition logic.
   */
  public function getLogic();

  /**
   * Sets the logic for this condition.
   *
   * @param string $logic
   *   The logic of this condition.
   *
   * @return $this
   */
  public function setLogic($logic);

  /**
   * Returns the operator for this condition.
   *
   * @return string
   *   A condition operator.
   */
  public function getOperator();

  /**
   * Sets the operator for this condition.
   *
   * @param string $operator
   *   The operator of this condition.
   *
   * @return $this
   */
  public function setOperator($operator);

  /**
   * Returns the values for this condition.
   *
   * @return string
   *   A condition values.
   */
  public function getValues();

  /**
   * Sets the values for this condition.
   *
   * @param string $values
   *   The values of this condition.
   *
   * @return $this
   */
  public function setValues($values);

  /**
   * Returns the weight of this persona (used for sorting).
   *
   * @return int
   *   The weight of this category.
   */
  public function getWeight();

  /**
   * Sets the weight.
   *
   * @param int $weight
   *   The desired weight.
   *
   * @return $this
   */
  public function setWeight($weight);

}
