<?php

namespace Drupal\persona\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\persona\PersonaInterface;
use Drupal\persona\Controller\PersonaControllerBase;

/**
 * Returns responses for Persona Condition module routes.
 */
class PersonaConditionController extends ControllerBase {

  /**
   * The entity storage.
   *
   * @var \Drupal\Core\Config\Entity\ConfigEntityStorage
   */
  protected $entityStorage;

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityManager;

  /**
   * Constructs a new DomainControllerBase.
   *
   * @param \Drupal\Core\Entity\EntityStorageInterface $entity_storage
   *   The storage controller.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_manager
   *   The entity manager.
   */
  public function __construct(EntityStorageInterface $entity_storage, EntityTypeManagerInterface $entity_manager) {
    $this->entityStorage = $entity_storage;
    $this->entityManager = $entity_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $entity_manager = $container->get('entity_type.manager');
    return new static(
      $entity_manager->getStorage('persona'),
      $entity_manager
    );
  }

  /**
   * Provides the persona condition submission form.
   *
   * @param \Drupal\persona\PersonaInterface $persona
   *   A persona entity.
   *
   * @return array
   *   Returns the persona condition submission form.
   */
  public function addCondition(PersonaInterface $persona) {
    // The entire purpose of this controller is to add the values from
    // the parent persona entity.
    $values['persona'] = $persona->id();

    // For now, we auto-set the label and group as well.
    $values['label'] = md5($persona->id() . microtime());
    $values['group'] = 'AND_1';
    $values['logic'] = 'AND';
    // Create the entity and pass to the form.
    $condition = \Drupal::entityTypeManager()->getStorage('persona_condition')->create($values);

    return $this->entityFormBuilder()->getForm($condition);
  }

  /**
   * Provides the listing page for conditiones.
   *
   * @param \Drupal\persona\PersonaInterface $persona
   *   A persona record entity.
   *
   * @return array
   *   A render array as expected by drupal_render().
   */
  public function listing(PersonaInterface $persona) {
    $list = \Drupal::entityTypeManager()->getListBuilder('persona_condition');
    $list->setPersona($persona);
    return $list->render();
  }

  public function title(PersonaInterface $persona) {
    return $persona->label();
  }

}
