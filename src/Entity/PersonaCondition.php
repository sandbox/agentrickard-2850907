<?php

namespace Drupal\persona\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\persona\PersonaConditionInterface;

/**
 * Defines the persona condition entity.
 *
 * @ConfigEntityType(
 *   id = "persona_condition",
 *   label = @Translation("Persona condition"),
 *   handlers = {
 *     "access" = "Drupal\persona\PersonaAccessControlHandler",
 *     "list_builder" = "Drupal\persona\PersonaConditionListBuilder",
 *     "form" = {
 *       "add" = "Drupal\persona\PersonaConditionForm",
 *       "edit" = "Drupal\persona\PersonaConditionForm",
 *       "default" = "Drupal\persona\PersonaConditionForm",
 *       "delete" = "Drupal\persona\Form\PersonaConditionDeleteForm"
 *     }
 *   },
 *   config_prefix = "condition",
 *   admin_permission = "administer personas",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "weight" = "weight"
 *   },
 *   links = {
 *     "delete-form" = "/admin/config/persona/condtion/delete/{persona_condition}",
 *     "edit-form" = "/admin/config/persona/condtion/edit/{persona_condition}",
 *     "collection" = "/admin/config/persona/condtion/{persona}",
 *     "canonical" = "/admin/config/persona/condtion/edit/{persona_condition}",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "persona",
 *     "plugin",
 *     "group",
 *     "logic",
 *     "operator",
 *     "values",
 *     "weight",
 *   }
 * )
 */
class PersonaCondition extends ConfigEntityBase implements PersonaConditionInterface {

  /**
   * The form ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The human-readable label of the persona.
   *
   * @var string
   */
  protected $label;

  /**
   * The plugin prividing the condition.
   *
   * @var string
   */
  protected $plugin;

  /**
   * The condition group.
   *
   * @var int
   */
  protected $group;

  /**
   * The condition logic for testing conditions (AND/OR).
   *
   * @var string
   */
  protected $logic;

  /**
   * The condition operator for testing conditions (= / <>).
   *
   * @var string
   */
  protected $operator;

  /**
   * The assigned values of the condition.
   *
   * @var array
   */
  protected $values;

  /**
   * The sort order.
   *
   * @var integer
   */
  protected $weight;

  /**
   * {@inheritdoc}
   */
  public function getPersona() {
    return $this->persona;
  }

  /**
   * {@inheritdoc}
   */
  public function setPersona($id) {
    $this->persona = $id;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getPlugin() {
    return $this->plugin;
  }

  /**
   * {@inheritdoc}
   */
  public function setPlugin($plugin) {
    $this->plugin = $plugin;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getGroup() {
    return $this->group;
  }

  /**
   * {@inheritdoc}
   */
  public function setGroup($group) {
    $this->group = $group;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getLogic() {
    return $this->logic;
  }

  /**
   * {@inheritdoc}
   */
  public function setLogic($logic) {
    $this->logic = $logic;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOperator() {
    return $this->operator;
  }

  /**
   * {@inheritdoc}
   */
  public function setOperator($operator) {
    $this->operator = $operator;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getValues() {
    return $this->values;
  }

  /**
   * {@inheritdoc}
   */
  public function setValues($values) {
    $this->values = $values;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getWeight() {
    return $this->weight;
  }

  /**
   * {@inheritdoc}
   */
  public function setWeight($weight) {
    $this->weight = $weight;
    return $this;
  }

}
