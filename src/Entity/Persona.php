<?php

namespace Drupal\persona\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\persona\PersonaInterface;

/**
 * Defines the persona entity.
 *
 * @ConfigEntityType(
 *   id = "persona",
 *   label = @Translation("Persona"),
 *   handlers = {
 *     "access" = "Drupal\persona\PersonaAccessControlHandler",
 *     "list_builder" = "Drupal\persona\PersonaListBuilder",
 *     "form" = {
 *       "add" = "Drupal\persona\PersonaForm",
 *       "edit" = "Drupal\persona\PersonaForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm"
 *     }
 *   },
 *   config_prefix = "persona",
 *   admin_permission = "administer personas",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label"
 *   },
 *   links = {
 *     "delete-form" = "/admin/config/persona/delete/{persona}",
 *     "edit-form" = "/admin/config/persona/edit/{persona}",
 *     "collection" = "/admin/config/persona",
 *     "canonical" = "/admin/config/persona/edit/{persona}",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *     "weight",
 *   }
 * )
 */
class Persona extends ConfigEntityBase implements PersonaInterface {

  /**
   * The form ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The human-readable label of the persona.
   *
   * @var string
   */
  protected $label;

  /**
   * The description of the persona.
   *
   * @var string
   */
  protected $description;

  /**
   * The sort order.
   *
   * @var integer
   */
  protected $weight;

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description) {
    $this->description = $description;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getWeight() {
    return $this->weight;
  }

  /**
   * {@inheritdoc}
   */
  public function setWeight($weight) {
    $this->weight = $weight;
    return $this;
  }

}
