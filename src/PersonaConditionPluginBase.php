<?php

/**
 * @file
 * Contains \Drupal\persona\PersonaConditionPluginBase.
 */

namespace Drupal\persona;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\persona\PersonaCondition\PluginInterface;

/**
 * Base methods for use with persona condition plugins.
 */
abstract class PersonaConditionPluginBase extends PluginBase implements PersonaConditionPluginInterface {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function id() {
    return $this->pluginDefinition['id'];
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    return $this->pluginDefinition['label'];
  }

  /**
   * @inheritdoc
   */
  public function options() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function status() {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function multiple() {
    return TRUE;
  }

  /**
   * @inheritdoc
   */
  public function operators() {
    return [
      '=' => $this->t('Equal to'),
      '<>' => $this->t('Not equal to'),
      '>' => $this->t('Greater than'),
      '<' => $this->t('Less than'),
      '>=' => $this->t('Greater than or equal to'),
      '<=' => $this->t('Less than or equal to'),
    ];
  }

  /**
   * @inheritdoc
   */
  public function applies(PersonaConditionInterface $condition) {
    return TRUE;
  }

  /**
   * @inheritdoc
   */
  public function configForm(PersonaConditionInterface $condition) {
    $form[$this->id()] = array(
      '#type' => 'container',
      '#states' => array(
        'visible' => array(
          ':input[name=plugin]' => array('value' => $this->id()),
        ),
      ),
    );
    $form[$this->id()][$this->id() . ':operator'] = array(
      '#type' => 'select',
      '#title' => $this->t('Comparison operator'),
      '#options' => $this->operators(),
      '#default_value' => $condition->getOperator(),
    );
    $form[$this->id()][$this->id() . ':values'] = array(
      '#type' => 'select',
      '#title' => $this->t('Value'),
      '#options' => $this->options(),
      '#multiple' => $this->multiple(),
      '#size' => ($this->multiple() && $this->options() >= 10) ? 10 : NULL,
      '#default_value' => $condition->getValues(),
    );
    return $form;
  }

  /**
   * @inheritdoc
   */
  public function cacheContexts() {
    return [];
  }

  /**
   * @inheritdoc
   */
  public function getMaxAge(PersonaConditionInterface $condition) {
    return -1;
  }

}
