<?php

namespace Drupal\persona;

use Drupal\Core\Config\Entity\DraggableListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\persona\PersonaInterface;

/**
 * User interface for the persona overview screen.
 */
class PersonaConditionListBuilder extends DraggableListBuilder {

  /**
   * {@inheritdoc}
   */
  protected $entitiesKey = 'persona_conditions';

  /**
   * Name of the entity's weight field or FALSE if no field is provided.
   *
   * @var string|bool
   */
  protected $weightKey = 'weight';

  /**
   * Sets the persona context for this list.
   *
   * @param \Drupal\persona\PersonaInterface $persona
   *   The persona to set as context for the list.
   */
  public function setPersona(PersonaInterface $persona) {
    $this->persona = $persona;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'persona_condition_overview_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['plugin'] = $this->t('Plugin');
    $header['group'] = $this->t('Group');
    $header['logic'] = $this->t('Logic');
    $header['operator'] = $this->t('Operator');
    $header['values'] = $this->t('Values');
    $header += parent::buildHeader();
    return $header;
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    // @TODO: Sanitize output.
    // @TODO: replace with plugin label.
    $row['plugin'] = ['#markup' => $entity->getPlugin()];
    $row['group'] = ['#markup' => $entity->getGroup()];
    $row['logic'] = ['#markup' => $entity->getLogic()];
    $row['operator'] = ['#markup' => $entity->getOperator()];
    $string = implode(', ', $entity->getValues());
    if (strlen($string) > 40) {
      $string = substr($string, 0, 40) . '...';
    }
    $row['values'] = ['#markup' => htmlentities($string)];
    $row += parent::buildRow($entity);
    return $row;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $groups = $this->getConditionGroups();
    if (count($groups) > 1) {
      // Do something with the form.
    }
    $form[$this->entitiesKey]['#personas'] = $this->entities;
    $form['actions']['submit']['#value'] = $this->t('Save configuration');
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    drupal_set_message($this->t('Configuration saved.'));
  }

  /**
   * {@inheritdoc}
   *
   * See Drupal\Core\Entity\EntityListBuilder::getEntityIds()
   *
   * @return array
   *   An array of entity IDs.
   */
  protected function getEntityIds() {
    $query = $this->getStorage()->getQuery()
      ->condition('persona', $this->persona->id())
      ->sort($this->entityType->getKey('weight'));
    // Only add the pager if a limit is specified.
    if ($this->limit) {
      $query->pager($this->limit);
    }
    return $query->execute();
  }

  /**
   * Gets the groups used by this persona.
   */
  protected function getConditionGroups() {
    $groups = [];
    foreach ($this->entities as $condition) {
      $name = $condition->getGroup();
      $values = explode('_', $name);
      $groups[$name] = [
        'condition' => $values[0],
        'weight' => $values[1]
      ];
    }
    return $groups;
  }

}
