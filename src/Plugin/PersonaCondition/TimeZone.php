<?php

/**
 * @file
 * Contains \Drupal\persona\Plugin\PersonaCondition\TimeZone.
 */

namespace Drupal\persona\Plugin\PersonaCondition;

use Drupal\persona\Annotation\PersonaCondition;
use Drupal\persona\PersonaInterface;
use Drupal\persona\PersonaConditionInterface;
use Drupal\persona\PersonaConditionPluginBase;
use Drupal\persona\PersonaConditionPluginInterface;

/**
 * Defines a condition for timezone differences.
 *
 * @PersonaCondition(
 *   id = "timezone",
 *   label = @Translation("Timezone"),
 *   modules = {},
 *   description = @Translation("Show content based on user timezone.")
 * )
 */
class TimeZone extends PersonaConditionPluginBase {

  /**
   * @inheritdoc
   */
  public function options() {
    return system_time_zones();
  }

  /**
   * @inheritdoc
   */
  public function operators() {
    return [
      '=' => $this->t('Equal to'),
      '<>' => $this->t('Not equal to'),
    ];
  }

  /**
   * @inheritdoc
   */
  public function cacheContexts() {
    return [
      'timezone',
    ];
  }

  /**
   * @inheritdoc
   */
  public function applies(PersonaConditionInterface $condition) {
    $timezone = drupal_get_user_timezone();
    $values = $condition->getValues();
    // @TODO: This is a simple string check, not a check that the timezones are equal.
    // For instance American/New York and America/Detroit are both Eastern Time.
    // Perhaps that belongs in a different plugin, though.
    // See http://php.net/date_default_timezone_get.
    $check = in_array($timezone, $values);
    $operator = $condition->getOperator();
    return ($operator == '=') ? $check : !$check;
  }

}
