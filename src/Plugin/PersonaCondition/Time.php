<?php

/**
 * @file
 * Contains \Drupal\persona\Plugin\PersonaCondition\Time.
 */

namespace Drupal\persona\Plugin\PersonaCondition;

use DateTime;
use Drupal\persona\Annotation\PersonaCondition;
use Drupal\persona\PersonaInterface;
use Drupal\persona\PersonaConditionInterface;
use Drupal\persona\PersonaConditionPluginBase;
use Drupal\persona\PersonaConditionPluginInterface;

/**
 * Defines a condition for time of day placement.
 *
 * @PersonaCondition(
 *   id = "time",
 *   label = @Translation("Time of day"),
 *   modules = {},
 *   description = @Translation("Show content based on time of day.")
 * )
 */
class Time extends PersonaConditionPluginBase {

  /**
   * @inheritdoc
   */
  public function options() {
    $options = [];
    for ($i = 0; $i <= 23; $i++) {
      foreach ([':00', ':15', ':30', ':45'] as $time) {
        $options[$i . $time] = $i . $time;
      }
    }
    return $options;
  }

  /**
   * @inheritdoc
   */
  public function multiple() {
    return FALSE;
  }

  /**
   * @inheritdoc
   */
  public function operators() {
    return [
      '>=' => $this->t('Greater than or equal to'),
      '<=' => $this->t('Less than or equal to'),
    ];
  }

  /**
   * @inheritdoc
   */
  public function getMaxAge(PersonaConditionInterface $condition) {
    // Time cache based on offset from current request.
    $operator = $condition->getOperator();
    $values = $condition->getValues();
    $value = current($values);
    $config_time = date('U', strtotime($value));
    $diff = REQUEST_TIME - $config_time;
    // If the time hasn't passed yet, cache until that time.
    if ($diff > 0) {
      $max_age = $diff;
    }
    else {
      // Set max age to tomorrow minus the offset.
      $max_age = (24*60*60) + $diff;
    }
    return $max_age;
  }

}
