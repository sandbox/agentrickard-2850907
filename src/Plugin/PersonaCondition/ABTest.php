<?php

/**
 * @file
 * Contains \Drupal\persona\Plugin\PersonaCondition\ABTest.
 */

namespace Drupal\persona\Plugin\PersonaCondition;

use Drupal\persona\Annotation\PersonaCondition;
use Drupal\persona\PersonaInterface;
use Drupal\persona\PersonaConditionInterface;
use Drupal\persona\PersonaConditionPluginBase;
use Drupal\persona\PersonaConditionPluginInterface;

/**
 * Defines a condition for random placement.
 *
 * @PersonaCondition(
 *   id = "abtest",
 *   label = @Translation("A/B Test"),
 *   modules = {},
 *   description = @Translation("Show content to a specific percentage of visitors.")
 * )
 */
class ABTest extends PersonaConditionPluginBase {

  /**
   * @inheritdoc
   */
  public function options() {
    return [
      '0.25' => $this->t('25%'),
      '0.33' => $this->t('33%'),
      '0.50' => $this->t('50%'),
      '0.67' => $this->t('67%'),
      '0.75' => $this->t('75%'),
    ];
  }

  /**
   * @inheritdoc
   */
  public function multiple() {
    return FALSE;
  }

  /**
   * @inheritdoc
   */
  public function operators() {
    return [
      '=' => $this->t('Equal to'),
    ];
  }

  /**
   * @inheritdoc
   */
  public function getMaxAge(PersonaConditionInterface $condition) {
    // A-B tests cannot be cached.
    return 0;
  }

}
