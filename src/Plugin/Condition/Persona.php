<?php

namespace Drupal\persona\Plugin\Condition;

use Drupal\Component\Utility\Unicode;
use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Condition\ConditionPluginBase;
use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\persona\PersonaConditionManagerInterface;
use Drupal\persona\PersonaInterface;
use Drupal\persona\PersonaLegacyConditionManager;

/**
 * Provides a 'Persona' condition.
 *
 * @Condition(
 *   id = "persona",
 *   label = @Translation("Persona")
 * )
 */
class Persona extends ConditionPluginBase implements ContainerFactoryPluginInterface, CacheableDependencyInterface {

  /**
   * The persona storage handler.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $personaStorage;

  /**
   * The condition storage handler.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $conditionStorage;

  /**
   * The condition manager.
   *
   * @var \Drupal\persona\PersonaConditionManagerInterface
   */
  protected $manager;

  /**
   * The legacy condition manager.
   *
   * @var \Drupal\persona\PersonaLegacyConditionManager
   */
  protected $legacyManager;

  /**
   * The active personas.
   */
  protected $personas;

  /**
   * The active cache contexts.
   */
  protected $contexts;

  /**
   * The time-sensitive conditions.
   */
  protected $timedConditions;

  /**
   * Constructs a Persona condition plugin.
   *
   * @param \Drupal\Core\Entity\EntityManagerInterface $entity_manager
   *   The entity manager class.
   * @param \Drupal\persona\PersonaConditionManagerInterface $manager
   *   The plugin manager.
   * @param \Drupal\persona\PersonaLegacyConditionManager $legacy_manager
   *   The legacy plugin manager.
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   */
  public function __construct(EntityManagerInterface $entity_manager, PersonaConditionManagerInterface $manager,  PersonaLegacyConditionManager $legacy_manager, array $configuration, $plugin_id, array $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->personaStorage = $entity_manager->getStorage('persona');
    $this->conditionStorage = $entity_manager->getStorage('persona_condition');
    $this->manager = $manager;
    $this->legacyManager = $legacy_manager;
    $this->personas = $this->personaStorage->loadMultiple();
    $this->contexts = array();
    $this->timedConditions = array();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
        $container->get('entity.manager'),
        $container->get('plugin.manager.persona.condition'),
        $container->get('plugin.manager.persona.legacy_condition'),
        $configuration,
        $plugin_id,
        $plugin_definition
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['personas'] = array(
      '#type' => 'checkboxes',
      '#title' => $this->t('When the following personas are active'),
      '#default_value' => $this->configuration['personas'],
      '#options' => array_map('\Drupal\Component\Utility\Html::escape', $this->getOptions()),
      '#description' => $this->t('If you select no personas, the condition will evaluate to TRUE for all requests.'),
      '#attached' => array(
        'library' => array(
          'persona/drupal.persona',
        ),
      ),
    );
    return parent::buildConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return array(
      'personas' => array(),
    ) + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['personas'] = array_filter($form_state->getValue('personas'));
    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function summary() {
    // Use the persona labels. They will be sanitized below.
    $personas = array_intersect_key($this->getOptions(), $this->configuration['personas']);
    if (count($personas) > 1) {
      $personas = implode(', ', $personas);
    }
    else {
      $personas = reset($personas);
    }
    if ($this->isNegated()) {
      return $this->t('Active persona is not @personas', array('@personas' => $personas));
    }
    else {
      return $this->t('Active persona is @personas', array('@personas' => $personas));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function evaluate() {
    $ids = $this->configuration['personas'];
    if (empty($ids) && !$this->isNegated()) {
      return TRUE;
    }
    // No context found?
    elseif (empty($ids)) {
      return FALSE;
    }

    // Load the personas.
    $personas = $this->personaStorage->loadMultiple($ids);
    uasort($personas, [$this, 'sort']);

    // Load the conditions for the persona into logical groups.
    foreach ($personas as $persona) {
      $conditions = $this->getConditions($persona);
      $groups = $this->groupConditions($conditions);
    }

    // Query the plugin for active status.
    $return = FALSE;
    $definitions = $this->manager->getDefinitions();
    $legacy_definitions = $this->legacyManager->getDefinitions();
    foreach ($groups as $group => $logic) {
      foreach ($logic as $operator => $conditions) {
        foreach ($conditions as $condition) {
          $id = $condition->getPlugin();
          // @TODO: Conversion handler for legacy Condition plugins.
          if (isset($definitions[$id])) {
            $handler = $this->manager->getPlugin($condition->getPlugin($id));
            // Add the cache contexts.
            $this->addCacheContexts($handler->cacheContexts());
            $applies = $handler->applies($condition);
            // Contexts cannot set the cache maxAge, but we can set them through the
            // CacheableDependencyInterface on the condition, so collect cache data
            // from each condition and apply if needed.
            $timed_cache = $handler->getMaxAge($condition);
            if ($timed_cache > -1) {
              $this->timedConditions[] = $timed_cache;
            }
          }
          elseif (isset($legacy_definitions[$id])) {
            // Handles legacy conditions.
            $configuration = $this->legacyManager->getConfiguration($condition, $this->legacyManager->createInstance($id));
            $handler = $this->legacyManager->createInstance($id, $configuration);
            $applies = $handler->evaluate();
          }
          if ($operator == 'AND') {
            $return = $applies;
          }
          elseif($operator == 'OR' && !empty($applies)) {
            $return = $applies;
          }
        }
      }
    }

    // NOTE: The context system handles negation for us.
    return $return;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    $contexts = parent::getCacheContexts();
    $contexts += $this->contexts;
    return $contexts;
  }

  /**
   * Returns an options list of personas.
   *
   * @return array
   *   An array of id => label for use in forms.
   */
  private function getOptions() {
    $options = [];
    uasort($this->personas, [$this, 'sort']);
    foreach ($this->personas as $persona) {
      $options[$persona->id()] = $persona->label();
    }
    return $options;
  }

  /**
   * Sorts persona options by weight and label.
   */
  private function sort($a, $b) {
    if ($a->getWeight() > $b->getWeight()) {
      return 1;
    }
    if ($a->label() > $b->label()) {
      return 1;
    }
    return 0;
  }

  /**
   * Returns an array of conditions that define a persona.
   *
   * @param \Drupal\persona\PersonaInterface $persona
   *   The persona being loaded.
   *
   * @return array
   *   An array of persona conditions. \Drupal\persona\PersonaConditionInterface
   */
  private function getConditions(PersonaInterface $persona) {
    $conditions = [];
    $query = $this->conditionStorage->getQuery()
      ->condition('persona', $persona->id())
      ->sort('weight');
    $ids = $query->execute();
    if (!empty($ids)) {
      $conditions = $this->conditionStorage->loadMultiple($ids);
      uasort($conditions, [$this, 'sort']);
    }
    return $conditions;
  }

  /**
   * Split our list of conditions into logical groups.
   *
   * @param array $conditions
   *
   * @return array
   */
  public function groupConditions($conditions) {
    $groups = [];
    foreach ($conditions as $condition) {
      $groups[$condition->getGroup()][$condition->getLogic()][] = $condition;
    }
    return $groups;
  }

  /**
   * {@inheritdoc}
   */
  public function addCacheContexts(array $contexts) {
    $this->contexts += $contexts;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    // Note that these cache settings apply to the access method on each block.
    // Default no cache.
    $age = -1;
    foreach ($this->timedConditions as $time) {
      if ($age == -1) {
        $age = $time;
      }
      elseif ($time < $age) {
        $age = $time;
      }
    }
    return $age;
  }

}
