<?php

namespace Drupal\persona;

use Drupal\persona\PersonaInterface;
use Drupal\persona\PersonaConditionInterface;

/**
 * Defines the plugin interface for persona conditions.
 */
interface PersonaConditionPluginInterface {

  /**
   * Returns the id for a plugin.
   *
   * @return string
   */
  public function id();

  /**
   * Returns the label for a plugin.
   *
   * @return string
   */
  public function label();

  /**
   * Returns the status of a plugin.
   *
   * @return boolean
   */
  public function status();

  /**
   * Checks if a condition matches applies to a given context.
   *
   * @param Drupal\persona\PersonaConditionInterface $condition
   *   The condition being checked.
   *
   * @return boolean
   */
  public function applies(PersonaConditionInterface $condition);

  /**
   * Gets the options for a plugin.
   *
   * @return array
   *   In the format id => label.
   */
  public function options();

  /**
   * Returns the if multiple selections are allowed; defaults to TRUE.
   *
   * @return boolean
   */
  public function multiple();

  /**
   * Gets the comparison operators for a plugin.
   *
   * @return array
   *   In the format id => label.
   */
  public function operators();

  /**
   * Provides configuration options.
   *
   * @param Drupal\persona\PersonaConditionInterface $condition;
   *   The persona the condition is attached to.
   */
  public function configForm(PersonaConditionInterface $condition);

  /**
   * Gets the cache contexts that apply to a condition.
   *
   * @return array
   *   An array of cacheContexts.
   */
  public function cacheContexts();

  /**
   * Gets the max cache time of the condition.
   *
   * @return int
   */
  public function getMaxAge(PersonaConditionInterface $condition);

}
