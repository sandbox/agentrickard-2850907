<?php

namespace Drupal\persona\Form;

use Drupal\Core\Entity\EntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Builds the form to delete a persona record.
 */
class PersonaConditionDeleteForm extends EntityConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete %name?', array('%name' => $this->entity->getPlugin()));
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('persona.condition_collection', ['persona' => $this->entity->getPersona()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->entity->delete();
    drupal_set_message($this->t('Condition %plugin has been deleted.', array('%plugin' => $this->entity->getPlugin())));
    \Drupal::logger('persona')->notice('Condition %plugin has been deleted.', array('%plugin' => $this->entity->getPlugin()));
    $form_state->setRedirectUrl($this->getCancelUrl());
  }

}
