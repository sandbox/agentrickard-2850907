<?php

/**
 * @file
 * Contains \Drupal\persona\PersonaLegacyConditionManager.
 */

namespace Drupal\persona;

use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\persona\PersonaConditionInterface;

class PersonaLegacyConditionManager extends DefaultPluginManager {

  /**
   * Constructs a new PersonaLegacyConditionManager.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/Condition', $namespaces, $module_handler, 'Drupal\Core\Plugin\ContainerFactoryPluginInterface', 'Drupal\Core\Condition\Annotation\Condition');
    $this->alterInfo('persona_legacy_condition_info');
    $this->setCacheBackend($cache_backend, 'persona_legacy_plugins');
    $this->moduleHandler = $module_handler;
  }

  public function getConfiguration(PersonaConditionInterface $condition, $plugin) {
    $this->plugin = $plugin;
    $configuration = [];
    $configuration['negate'] = (bool) ($condition->getOperator() == '<>');
    $keys = $this->getValueKey();
    $type = $this->getDataType();
    foreach ($keys as $key) {
      $configuration[$key] = ($type == 'array') ? $condition->getValues() : implode('', $condition->getValues());
    }
    return $configuration;
  }

  public function getValueKey() {
    $config = $this->plugin->defaultConfiguration();
    if (isset($config['negate'])) {
      unset($config['negate']);
    }
    return array_keys($config);
  }

  public function getDataType() {
    $config = $this->plugin->defaultConfiguration();
    // This is brittle.
    $key = current($config);
    return gettype($key);
  }

}
